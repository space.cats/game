import { Scene } from "phaser";

export class CatBaseScene extends Scene{
    
    getCenterPoints(){
        return {x:this.cameras.main.centerX, y:this.cameras.main.centerY}
    }
}
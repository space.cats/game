export class PlanetCreationSteps {
    static BODY_SELECT = 0
    static PIC_SELECT = 1
    static MOON_SELECT = 2

    static getNextStep(step: number = null){
        return step+1;
    }

    static isDone(step: number){
        return step > PlanetCreationSteps.MOON_SELECT;
    }
}

export class PlanetCreationController {
    static BODY_SELECT_OPTIONS = [
        {
            text: 'Rocky',
            callback: (scene) => {
                window.dispatchEvent(new CustomEvent('planetBodySelected', {detail: 'rock'}))
            }
        },
        {
            text: 'Icy',
            callback: (scene) => {
                window.dispatchEvent(new CustomEvent('planetBodySelected', {detail: 'ice'}))
            }
        },
        {
            text: 'Gas Giant',
            callback: (scene) => {
                window.dispatchEvent(new CustomEvent('planetBodySelected', {detail: 'gas'}))
            }
        },
    ]

    static PIC_SELECT_OPTIONS = [
        {
            text: 'Style 1',
            callback: (scene) => {
                window.dispatchEvent(new CustomEvent('planetStyleSelected', {detail: 1}))
            }
        },
        {
            text: 'Style 2',
            callback: (scene) => {
                window.dispatchEvent(new CustomEvent('planetStyleSelected', {detail: 2}))
            }
        },
        {
            text: 'Style 3',
            callback: (scene) => {
                window.dispatchEvent(new CustomEvent('planetStyleSelected', {detail: 3}))
            }
        },
    ]

    static MOON_SELECT_OPTIONS = [
        {
            text: 'No moons',
            callback: (scene) => {
                window.dispatchEvent(new CustomEvent('planetMoonCountSelected', {detail: 0}))
            }
        },
        {
            text: 'One moon',
            callback: (scene) => {
                window.dispatchEvent(new CustomEvent('planetMoonCountSelected', {detail: 1}))
            }
        },
    ]

    static getStepOptions(step: number) {
        switch (step) {
            case PlanetCreationSteps.BODY_SELECT:
                return PlanetCreationController.BODY_SELECT_OPTIONS
                ;;
            case PlanetCreationSteps.PIC_SELECT:
                return PlanetCreationController.PIC_SELECT_OPTIONS
                ;;
            case PlanetCreationSteps.MOON_SELECT:
                return PlanetCreationController.MOON_SELECT_OPTIONS
                ;;
        }
    }

}
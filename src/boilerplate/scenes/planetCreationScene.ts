import { PlanetDraft } from '../objects/planetDraft';
import ButtonMixin from '../utils/buttonMixin';
import { Scene } from 'phaser';
import { PlanetCreationController, PlanetCreationSteps } from './controllers/planetCreationController';
import { PlanetarySystemScene } from './planetarySystemScene';

export class PlanetCreationScene extends ButtonMixin {
    static key = 'PlanetCreationScene'

    planet: PlanetDraft

    step: number = PlanetCreationSteps.BODY_SELECT
    selectors = []

    needToMakeButtons: boolean = true
    isPanned: boolean = false

    SELECTOR_CONFIG = [
        {
            x: 100,
            y: 200,
        },
        {
            x: 100,
            y: 300,
        },
        {
            x: 100,
            y: 400,
        },
        {
            x: 400,
            y: 900,
            text: 'Done!',
            callback: (scene: Scene) => this.scene.start('PlanetarySystemScene')
        },
    ]

    PANNED_SELECTOR_CONFIG = [
        {
            x: 900,
            y: 200,
        },
        {
            x: 900,
            y: 300,
        },
        {
            x: 900,
            y: 400,
        },
        {
            x: 900,
            y: 900,
            text: 'Done!',
            callback: (scene: Scene) => this.scene.start('PlanetarySystemScene')
        },
        
    ]

    constructor () {
        super({
            key: PlanetCreationScene.key
        })
    }

    preload () {
        this.load.image('pTx_r1', './src/boilerplate/assets/planets/PLANETASROCOSOS/PLANETAROCOSO1.png')
        this.load.image('pTx_r2', './src/boilerplate/assets/planets/PLANETASROCOSOS/PLANETAROCOSO2.png')
        this.load.image('pTx_r3', './src/boilerplate/assets/planets/PLANETASROCOSOS/PLANETAROCOSO3.png')
        this.load.image('pTx_i1', './src/boilerplate/assets/planets/PLANETASHIELO/PLANETAHIELO1.png')
        this.load.image('pTx_i2', './src/boilerplate/assets/planets/PLANETASHIELO/PLANETAHIELO2.png')
        this.load.image('pTx_i3', './src/boilerplate/assets/planets/PLANETASHIELO/PLANETAHIELO3.png')
        this.load.image('pTx_g1', './src/boilerplate/assets/planets/PLANETASGASEOSOS/PLANETAGASEOSO1.png')
        this.load.image('pTx_g2', './src/boilerplate/assets/planets/PLANETASGASEOSOS/PLANETAGASEOSO2.png')
        this.load.image('pTx_g3', './src/boilerplate/assets/planets/PLANETASGASEOSOS/PLANETAGASEOSONARANJA.png')
        // this.load.image('pTx_moon', './src/boilerplate/assets/planets/PLANETAROCOSO1.png')
    }

    create () {
        this.drawTitleCentered(100, "Create your own planet!");
        this.planet = PlanetDraft.createDraftPlanet(this)
        this.planet.setPosition(this.sys.game.canvas.width, this.sys.game.canvas.height/2)
        this.needToMakeButtons = true
    }

    update() {
        if (this.needToMakeButtons){
            this.textElements.map(el => this.children.remove(el))
            this.makeButtons()
            this.needToMakeButtons = false
        }
        if (this.isPanned){
            this.doPan(1, 0)
        } else (
            this.doPan(0, 0)
        )
    }

    makeButtons() {
        if (PlanetCreationSteps.isDone(this.step)){
            this.cameras.main.zoomTo(.001, 3000, 'Expo.easeOut')
            this.time.addEvent({
                delay: 3000,
                callback: () => {
                    window.localStorage.setItem('selected-texture', this.planet.texture.key)
                    this.scene.start('PlanetarySystemScene')
                }
            })
        } else {
            let options = PlanetCreationController.getStepOptions(this.step).entries();
            for (let [i, option] of options){
                let selectorCfg = !this.isPanned?this.SELECTOR_CONFIG[i]:this.PANNED_SELECTOR_CONFIG[i];
                this.addButton(
                    selectorCfg.x,
                    selectorCfg.y,
                    option.text,
                    () => {
                        option.callback(this)
                        this.step = PlanetCreationSteps.getNextStep(this.step)
                        this.isPanned = !this.isPanned
                        this.needToMakeButtons = true
                    }
                )
            }
        }
    }
}
import ButtonMixin from '../utils/buttonMixin';
import { StarSelectionScene } from './starSelectionScene';

export class StartScene extends ButtonMixin {
  private backgroundImage: Phaser.GameObjects.Image;
  private gatito: Phaser.GameObjects.Sprite;

  MAX_GATITO_SCALE: number = .5
  MIN_GATITO_SCALE: number = .3
  SMALL_GATITO = true

  constructor() {
    super({
      key: "StartScene"
    });
  }

  preload(): void {
    this.load.image("background", "./src/boilerplate/assets/backgrounds/splash.png");
    this.load.image("gatito", "./src/boilerplate/assets/sprites/gatito.png");
  }
  
  create(): void {
    this.backgroundImage = this.add.image(
      null, null,
      'background'
    )
    this.backgroundImage.setScale(.8)
    this.backgroundImage.setOrigin(.5, .6)
    this.backgroundImage.setPosition(
      this.sys.canvas.width/2,
      this.sys.canvas.height/2,
    )

    this.drawTitleCentered(
      this.sys.canvas.height*6/10,
      "Pet the kitty to start!"
    )
    this.gatito = this.add.sprite(this.sys.canvas.width/2, this.sys.canvas.height*7/10, 'gatito');
    this.gatito.setOrigin(.5, 0);
    this.gatito.setScale(this.MIN_GATITO_SCALE);
    this.gatito.setInteractive();
    this.gatito.on('pointerup', () => {
      this.scene.start(StarSelectionScene.key)
    })
  }

  update(): void {
    // if (this.gatito.scale >= this.MAX_GATITO_SCALE)
    //   Phaser.Actions.ScaleXY([this.gatito], -0.01)
    // else 
    //   Phaser.Actions.ScaleXY([this.gatito], 0.01)
  }
}
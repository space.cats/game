import { Planet } from "../objects/planet";
import { Star } from "../objects/star";
import { CatBaseScene } from "./baseScene";

/**
 * @author Eugenio Arosteguy <eugenio.arosteguy@gmail.com>
 */

export class PlanetarySystemScene extends CatBaseScene {
  star: Star;
  planets: Planet[]=[]
  numberOfPlanets= 10;

  constructor() {
    super({
      key: "PlanetarySystemScene"
    });
  }

  preload(): void {
    this.load.image('sun', './src/boilerplate/assets/sprites/sun.png');
    this.load.image('ball', './src/boilerplate/assets/sprites/blue_ball.png');
    this.load.image('fondo1', './src/boilerplate/assets/fondo1cortado2.png');        
  }

  create(): void {
    this.add.image(this.cameras.main.centerX,this.cameras.main.centerY,'fondo1');
    this.star = Star.createStar(this,this.cameras.main.centerX, this.cameras.main.centerY)

    this.createPlanets();
  }
  
  createPlanets(){
    let selectedTexture = window.localStorage.getItem('selected-texture');
    this.planets =[...Array(this.numberOfPlanets)].map(e=>new Planet(this));
    let lucky = Math.floor(this.numberOfPlanets*Math.random())
    this.planets[lucky].bornMoon()
    this.planets[lucky].setTexture(selectedTexture)
    this.planets[lucky].setScale(.1)

    this.planets[lucky].setInteractive()
    this.planets[lucky].on('pointerover', () => {
      this.add.text(100, 100, `Selected planet: ${selectedTexture}`)
    })
  }

  // update() {
  //   // Phaser.Actions.RotateAroundDistance(group.getChildren(), { x: 400, y: 300 }, 0.02, 200);
  //   this.planets.forEach(p=>p.orbit())
  // }

  // preRender() {
  // }

}

import { PlanetCreationScene } from './planetCreationScene';
import { Star } from '../objects/star';
import { Scene } from 'phaser';
import ButtonMixin from '../utils/buttonMixin';

export class StarSelectionScene extends ButtonMixin {
    static key = 'StarSelectionScene'

    starOptions:Star[]

    constructor () {
        super({
            key: StarSelectionScene.key
        })
    }

    preload () {
        this.load.image('sun1', './src/boilerplate/assets/suns/ESTRELLA1.png');
        this.load.image('sun2', './src/boilerplate/assets/suns/ESTRELLA2.png');
        this.load.image('sun3', './src/boilerplate/assets/suns/ESTRELLA3.png');
        this.load.image('sun4', './src/boilerplate/assets/suns/ESTRELLA4.png');
        this.load.image('fondo1', './src/boilerplate/assets/fondo1cortado.png');    
    }

    create () {
        this.add.image(this.cameras.main.centerX, this.cameras.main.centerY, 'fondo1');
        this.drawTitleCentered(100, "Select your host star!");
        const centerX = this.cameras.main.centerX;
        const centerY = this.cameras.main.centerY;

        this.starOptions =[...Array(8).keys()].map(i=>{
            const x = centerX/2 + (i%2) * centerX 
            const y = centerY/2 +  (Math.round((i+1)/2)-1) * (centerY/2)
            let star = Star.createStar(this,x,y)
            star.setScale(.6)
            star.rotationSpeed = (.01*Math.random() +.001)

            // star.addListener('onclick', () => this.scene.start(PlanetCreationScene.key))
            
            star.setInteractive().on('pointerup',() => this.scene.start(PlanetCreationScene.key));
            // TODO: FIXME: HACKME: falta programar scroll de la cámara para que se vean
            // lo do sole de abajo 
            return star
        });

        this.starOptions.forEach((star) => {
            this.drawText(
                star.x - star.width/5,
                star.y + star.height/6,
                "Random G-Class Star",
                {
                    backgroundColor: '#222'
                }
            )
        })

        this.addButton(
            this.sys.canvas.width - 40,
            this.sys.canvas.height - 60,
            'V',
            () => {
                this.doPan(0, 1);
            }
        )

        this.addButton(
            this.sys.canvas.width - 40,
            this.sys.canvas.height + 60,
            'A',
            () => {
                this.doPan(0, 0);
            }
        )
    }

    update() {

    }
}
import TextMixin from './textMixin'

class Button {
    BUTTON_PADDING = 10; // px
    BUTTON_BACKGROUND_COLOUR = '#222';

    text: string
    x: number
    y: number
    styleParams: any
    callback: Function

    constructor(x: number, y: number, text: string, s: any, c: Function) {
        this.x = x;
        this.y = y;
        this.text = text;
        this.styleParams = {
            ...s,
            backgroundColor: s.backgroundColor || this.BUTTON_BACKGROUND_COLOUR,
            padding: {
                x: this.BUTTON_PADDING,
                y: this.BUTTON_PADDING,
            },
            borderRadius: '5px',
        };
        this.callback = c;
    }

    addToScene(scene: TextMixin): Phaser.GameObjects.Text {
        let textElement = scene.drawText(this.x, this.y, this.text, this.styleParams);
        textElement.setInteractive().on('pointerup', this.callback)
        return textElement
    }
}

export default class ButtonMixin extends TextMixin {
    buttons: Button[] = [];
    textElements: Phaser.GameObjects.Text[] = [];

    addButton (x, y, text, callback: Function, style: any={}) {
        this.buttons.push(
            new Button(x, y, text, style, callback)
        );
        this.textElements = this.buttons.map(b => b.addToScene(this))
    }
}
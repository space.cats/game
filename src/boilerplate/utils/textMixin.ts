import ScrollableScene from "./scrollableScene";

export default class TextMixin extends ScrollableScene {
    TEXT_FONT_SIZE: number = 24;
    TITLE_FONT_SIZE: number = 72;

    textItems: Phaser.GameObjects.Text[] = [];

    drawText(x, y, data, style: any={}): Phaser.GameObjects.Text {
        if (!style.fontSize) {
            style.fontSize = `${this.TEXT_FONT_SIZE}px`;
        }
        let _style = {
            ...style,
            fontFamily: "Dosis",
        }
        let txt = this.add.text(x, y, data, _style);
        this.textItems.push(txt);
        return txt;
    }

    drawTitle(x, y, data, style: any={}): Phaser.GameObjects.Text {
        if (!style.fontSize) {
            style.fontSize = `${this.TITLE_FONT_SIZE}px`;
        }
        let _style = {
            ...style,
            fontFamily: "Big Shoulders Text, cursive",
        }
        let txt = this.add.text(x, y, data, _style);
        this.textItems.push(txt);
        return txt;
    }

    drawTitleCentered(y, data, style: any={}): Phaser.GameObjects.Text {
        let xpos = this.sys.game.canvas.width / 2;
        let title = this.drawTitle(xpos, y, data, style);
        title.setX(title.x - title.width/2);
        return title;
    }

    redrawTextItems(): void {
        this.textItems.map(el => this.add.existing(el));
    }
}
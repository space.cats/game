export default class ScrollableScene extends Phaser.Scene {
    gridX: number = 0;
    gridY: number = 0;

    protected doPan(toX: number, toY: number, duration: number = 200) {
        let canvasCenterX = this.sys.canvas.width / 2;
        let canvasCenterY = this.sys.canvas.height / 2;

        this.cameras.main.pan(
            canvasCenterX + this.sys.canvas.width * toX,
            canvasCenterY + this.sys.canvas.width * toY,
            duration,
            'Cubic.easeInOut'
        )

        this.gridX = toX;
        this.gridY = toY;
    }
}
import { Scene } from "phaser";
import { PlanetarySystemScene } from "../scenes/planetarySystemScene";

export class Star extends Phaser.GameObjects.Sprite{
    scene:PlanetarySystemScene
    rotationSpeed: number;
    
    static createStar(scene:Scene, x: number, y:number) {
      return new Star({scene:scene, key: `sun${Math.ceil(4*Math.random())}`, x:x, y:y});
      // this.physics.world.enable(this.ship);
    }

    constructor(params) {
        super(params.scene, params.x, params.y, params.key, params.frame);
        this.setScale(0.2)
        this.rotationSpeed = .03*Math.random()+.005;
        this.scene.add.existing(this);
    }
    preUpdate(){
      this.rotation -= this.rotationSpeed
    }
}
import { Scene } from "phaser";
import { PlanetarySystemScene } from "../scenes/planetarySystemScene";
import { PlanetCreationScene } from "../scenes/planetCreationScene";

export class Planet extends Phaser.GameObjects.Sprite {
    static maxDistance = 400
    distanceToCenter: number;
    orbitAngle: number;
    rotationSpeed: number;

    orbitCenter:Phaser.GameObjects.Sprite

    scene: PlanetarySystemScene
    size: number;

    follower;
    path;
    graphics;
  moon: Planet;

    constructor(scene:PlanetarySystemScene) {
      super(scene, null,null, 'pTx_'+(Math.random()>.5?'i':'r')+Phaser.Math.Between(1,3));
      this.orbitCenter = scene.star
      //set random values
      this.setRandomPosition();
      this.distanceToCenter = Math.min(25*Math.floor(5+Math.random()*16), Planet.maxDistance)
      // this.size = (Planet.maxDistance/this.distanceToStar)*3*Math.random()+1
      this.setScale(.15*Math.random()+.015);
      // this.orbitAngle = 0.1*Math.random()+0.01;
      this.orbitAngle = .003*(2*Planet.maxDistance/this.distanceToCenter)
      this.rotationSpeed = .03*Math.random()+.02

      this.scene.add.existing(this);
    }

    bornMoon(){
      this.moon = new Planet(this.scene)
      this.moon.orbitCenter = this
      this.moon.distanceToCenter = 50
      this.moon.orbitAngle = .08
      this.moon.setScale(.02);
    }

    preUpdate(){
      Phaser.Actions.RotateAroundDistance([this], { x: this.orbitCenter.x, y: this.orbitCenter.y }, this.orbitAngle, this.distanceToCenter);
      this.rotation += this.rotationSpeed
    }

}
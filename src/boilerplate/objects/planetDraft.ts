import { Scene } from 'phaser';

export class PlanetDraft extends Phaser.GameObjects.Sprite {

    PLANET_BOB_ANIMATION_DURATION_SECONDS = .5;
    PLANET_BOB_AMOUNT = .25;
    
    PLANET_ROTATION_SPEED = .005;

    PLANET_BASE_SCALE = 1.5;

    BOB_DIRECTION = 1;

    planetBody: string = 'rock'
    planetStyle: number = 1
    planetMoonCount: number = 0

    static createDraftPlanet(scene: Scene) {
        return new PlanetDraft({
            scene: scene,
            key: "pTx_r1",
            frame: null
        })
    }

    constructor (params) {
        super(params.scene, null, null, params.key, params.frame)
        this.scale = this.PLANET_BASE_SCALE
        this.setOrigin(.5, .5)

        this.scene.add.existing(this)

        window.addEventListener('planetBodySelected', (evt: CustomEvent) => {
            this.planetBody = evt.detail
            this.updateImage()
        })
        window.addEventListener('planetStyleSelected', (evt: CustomEvent) => {
            this.planetStyle = evt.detail
            this.updateImage()
        })
        window.addEventListener('planetMoonCountSelected', (evt: CustomEvent) => {
            this.planetMoonCount = evt.detail
            this.updateImage()
        })
    }

    updateImage() {
        this.setTexture(`pTx_${this.planetBody.substr(0,1)}${this.planetStyle}`)
    }

    preUpdate () {
        this.rotation += this.PLANET_ROTATION_SPEED;
        this.animateBob();
    }

    animateBob() {
        if (this.scale >= 1+this.PLANET_BOB_AMOUNT){
            this.BOB_DIRECTION = -1
        } else if (this.scale <= 1-this.PLANET_BOB_AMOUNT) {
            this.BOB_DIRECTION = 1
        }
        Phaser.Actions.SetScale(
            [this],
            this.PLANET_BASE_SCALE + this.BOB_DIRECTION*this.PLANET_BOB_AMOUNT,
            this.PLANET_BASE_SCALE + this.BOB_DIRECTION*this.PLANET_BOB_AMOUNT,
            .4, .4
        )
    }
}


import "phaser";
import { StartScene } from "./scenes/startScene";
import { PlanetCreationScene } from './scenes/planetCreationScene';
import { PlanetarySystemScene } from "./scenes/planetarySystemScene";
import { StarSelectionScene } from "./scenes/starSelectionScene";

// main game configuration
const config: Phaser.Types.Core.GameConfig = {
  width: 600,
  height: 1280,
  type: Phaser.AUTO,
  parent: "game",
  scene: [StartScene, StarSelectionScene, PlanetCreationScene, PlanetarySystemScene],
  // physics: {
  //   default: "arcade",
  //   arcade: {
  //     gravity: { y: 200 }
  //   }
  // }
};

// game class
export class Game extends Phaser.Game {
  constructor(config: Phaser.Types.Core.GameConfig) {
    super(config);
  }
}

// when the page is loaded, create our game instance
window.addEventListener("load", () => {
  var game = new Game(config);
});
